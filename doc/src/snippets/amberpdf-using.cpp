//! [1]
#include <amberpdf/pdfdocument.h>
//! [1]

//! [2]
auto documentProvider = new PdfDocument();
connect(documentProvider, &PdfDocument::statusChanged, this, [this](PdfDocument::DocumentStatus status) {
    if (status == BaseDocument::DocumentStatus::Ready) {
        // document load succesfully
    } else {
        qDebug() << "Document load error" << status;
    }
});

connect(documentProvider, &PdfDocument::pageCountChanged, this, [this](int pageCount) {
    if (status == BaseDocument::DocumentStatus::Ready) 
        qDebug() << "Document have" << pageCount << "pages";
});

documentProvider->setPath("path/to/the/pdf/document.pdf");
//! [2]

//! [3]
#include <amberpdf/pdfpage.h>
//! [3]

//! [4]
auto pageFuture = m_document->page(m_pageIndex);
pageFuture.waitForFinished();
if (pageFuture.isFinished() && !pageFuture.isCanceled())
    qDebug() << "page load successfuly";
//! [4]

//! [5]
auto watcher = new QFutureWatcher<QSharedPointer<PdfPage>>();
watcher->setFuture(m_pdfiumDocument->page(1));
connect(watcher, &QFutureWatcher<QSharedPointer<PdfPage>>::finished, this, [this]() {
    auto page = watcher->result();
});
//! [5]











