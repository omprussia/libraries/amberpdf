Name:    amberpdf
Summary: Qt library that renders PDF files using the pdfium.
Version: 1.0.0
Release: 1
License: BSD
URL:     https://developer.auroraos.ru/open-source
Source0: %{name}-%{version}.tar.bz2

BuildRequires: pkgconfig(Qt5Core)
BuildRequires: pkgconfig(Qt5Gui)
BuildRequires: pkgconfig(libjpeg)
BuildRequires: pkgconfig(icu-uc)
BuildRequires: pkgconfig(freetype2)
BuildRequires: pkgconfig(zlib)
BuildRequires: cmake
BuildRequires: ninja
BuildRequires: pdfium-devel >= 1.0.0

%description
This project is a PDFium wrapper that allows to use it with Qt.

%package devel
Summary: Development files for AmberPDF
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the files necessary to develop applications
that use the AmberPDF library.

%prep
%autosetup

%build
%cmake \
  -DAMBERPDF_LIBRARY_VERSION=%{version} \
  -G "Ninja"
%ninja_build

%install
%ninja_install

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%{_libdir}/libamberpdf.so.*
%license LICENSE.BSD-3-Clause.md

%files devel
%{_includedir}/amberpdf/
%{_libdir}/libamberpdf.so
%{_libdir}/pkgconfig/amberpdf.pc
