# Authors

* Alexey Fedchenko, <a.fedchenko@omp.ru>
  * Developer, 2020-2022
  * Maintainer, 2021-2022
* Anton Zernin, <a.zernin@omp.ru>
  * Reviewer, 2021-2022
* Kirill Chuvilin, <k.chuvilin@omp.ru>
  * Developer, 2020
  * Maintainer, 2020
  * Product owner, 2020-2022
  * Reviewer, 2020-2021
* Liliya Nasyrova, <l.nasyrova@omp.ru>
  * QA Engineer, 2020-2022
* Tatyana Kaznova, <t.kaznova@omp.ru>
  * Product manager, 2021-2022
* Valeriya Kuchaeva, <v.kuchaeva@omp.ru>
  * UI Designer, 2020-2022
* Yulia Stepanova, <y.stepanova@omp.ru>
  * Product manager, 2020-2021
* Andrej Begichev, <a.begichev@omp.ru>
  * Developer, 2024
