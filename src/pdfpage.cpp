/****************************************************************************
**
** Copyright (C) 2021 - 2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the AmberPDF project.
**
** $QT_BEGIN_LICENSE:BSD$
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform LLC copyright holder nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtAlgorithms>

#include "pdftaskqueue.h"
#include "pdfdocumentholder.h"
#include "tasks/pdfpageclosetask.h"
#include "tasks/pdfpageannotationloadtask.h"
#include "tasks/pdfpagerendertask.h"
#include "tasks/pdfpagewordstask.h"
#include "tasks/pdfpagesizetask.h"
#include "tasks/pdfpageaddannotationtask.h"
#include "tasks/pdfpageremoveannotationtask.h"
#include "tasks/pdfpageaddinkannotationtask.h"
#include "tasks/pdfpagedrawsolidpaths.h"
#include "tasks/pdfpageaddimagetask.h"
#include "tasks/pdfpageremovesearchresultannotationstask.h"

#include "pdfpage_p.h"
#include "pdfpage.h"

PdfPageData::PdfPageData() : m_pageNumber(-1) { }

PdfPageData::PdfPageData(int pageNumber, QSharedPointer<fpdf_page_t__> page,
                         QSharedPointer<PdfDocumentHolder> doc)
    : m_pageNumber(pageNumber), m_page(page), m_documentHolder(doc)
{
}

PdfPageData::PdfPageData(const PdfPageData &other)
    : QSharedData(other),
      m_pageNumber(other.m_pageNumber),
      m_page(other.m_page),
      m_documentHolder(other.m_documentHolder),
      m_originalSize(other.m_originalSize)
{
}

PdfPageData::PdfPageData(PdfPageData &&other)
    : QSharedData(qMove(other)),
      m_pageNumber(qMove(other.m_pageNumber)),
      m_page(qMove(other.m_page)),
      m_documentHolder(qMove(other.m_documentHolder)),
      m_originalSize(qMove(other.m_originalSize))
{
}

PdfPageData &PdfPageData::operator=(const PdfPageData &other)
{
    m_pageNumber = other.m_pageNumber;
    m_page = other.m_page;
    m_documentHolder = other.m_documentHolder;
    m_originalSize = other.m_originalSize;
    m_words = other.m_words;
    m_originalSize = other.m_originalSize;
    return *this;
}

PdfPageData &PdfPageData::operator=(PdfPageData &&other)
{
    qSwap(m_pageNumber, other.m_pageNumber);
    qSwap(m_page, other.m_page);
    qSwap(m_documentHolder, other.m_documentHolder);
    qSwap(m_originalSize, other.m_originalSize);
    qSwap(m_words, other.m_words);
    qSwap(m_originalSize, other.m_originalSize);
    return *this;
}

PdfPageData::~PdfPageData()
{
    if (!m_originalSize.isFinished())
        m_originalSize.cancel();

    auto wordsFuture = m_words.future();
    if (wordsFuture.isFinished()) {
        auto wordsList = wordsFuture.result();
        QMutableListIterator<QObject *> wordsIt(wordsList);
        while (wordsIt.hasNext())
            delete wordsIt.next();
    } else {
        wordsFuture.cancel();
    }

    auto closePageInterface = QFutureInterface<void>();
    auto *closePageTask = new PdfPageCloseTask(m_documentHolder, closePageInterface, m_page);
    closePageTask->run();
    delete closePageTask;

    closePageInterface.reportStarted();
    closePageInterface.future().waitForFinished();
}

PdfPage::PdfPage() : d(new PdfPageData()) { }

PdfPage::~PdfPage() = default;

int PdfPage::pageNumber() const
{
    return d->m_pageNumber;
}

bool PdfPage::isValid() const
{
    if (!d->m_documentHolder && !d->m_page)
        return false;

    return d->m_page;
}

QFuture<QSizeF> PdfPage::originalSize()
{
    if (d->m_originalSize.isFinished() || d->m_originalSize.isRunning())
        return d->m_originalSize.future();

    auto *task = new PdfPageSizeTask(d->m_pageNumber, d->m_documentHolder, d->m_originalSize);
    auto addingResult = PdfTaskQueue::instance().addTask(task, PdfTaskQueue::TaskPriority::High,
                                                         d->m_documentHolder->id());
    if (!addingResult) {
        delete task;
        return {};
    }

    d->m_originalSize.reportStarted();

    return d->m_originalSize.future();
}

QFuture<QImage> PdfPage::bitmapFull(qreal pageScale, int renderFlags) const
{
    if (!d->m_documentHolder || !d->m_page)
        return {};

    if (!d->m_originalSize.isFinished())
        return {};

    QFutureInterface<QImage> interface;
    const auto &future = interface.future();

    auto *task =
            new PdfPageRenderFullTask(pageScale, renderFlags, d->m_originalSize.future().result(),
                                      d->m_page, d->m_documentHolder, interface);
    auto addingResult = PdfTaskQueue::instance().addTask(task, PdfTaskQueue::TaskPriority::Low,
                                                         d->m_documentHolder->id());
    if (!addingResult) {
        delete task;
        return {};
    }

    interface.reportStarted();

    return future;
}

QFuture<QImage> PdfPage::bitmapPart(qreal pageScaleX, qreal pageScaleY, int renderFlags, qreal zoom,
                                    const QPointF &bias) const
{
    if (!d->m_documentHolder || !d->m_page)
        return {};

    if (!d->m_originalSize.isFinished())
        return {};

    QFutureInterface<QImage> interface;
    const auto &future = interface.future();

    auto *task = new PdfPageRenderPartTask(pageScaleX, pageScaleY, renderFlags, zoom, bias,
                                           d->m_originalSize.future().result(), d->m_page,
                                           d->m_documentHolder, interface);
    auto addingResult = PdfTaskQueue::instance().addTask(task, PdfTaskQueue::TaskPriority::Low,
                                                         d->m_documentHolder->id());
    if (!addingResult) {
        delete task;
        return {};
    }

    interface.reportStarted();

    return future;
}

QFuture<QList<QObject *>> PdfPage::annotations()
{
    if (!d->m_documentHolder || !d->m_page)
        return {};

    QFutureInterface<QList<QObject *>> interface;
    const auto &future = interface.future();

    auto *task = new PdfPageAnnotationTask(d->m_page, QList<QObject *>(), interface,
                                           d->m_documentHolder);
    auto addingResult = PdfTaskQueue::instance().addTask(task, PdfTaskQueue::TaskPriority::High,
                                                         d->m_documentHolder->id());
    if (!addingResult) {
        delete task;
        return {};
    }
    interface.reportStarted();

    return future;
}

QFuture<QList<QObject *>> PdfPage::words()
{
    if (!d->m_documentHolder || !d->m_page)
        return {};
    auto origSizeF = originalSize();
    origSizeF.waitForFinished();
    auto origSize = origSizeF.result();
    auto *task =
            new PdfPageWordsTask(d->m_page, QList<QObject *>(), origSize,
                                 d->m_words, d->m_documentHolder);
    auto addingResult = PdfTaskQueue::instance().addTask(task, PdfTaskQueue::TaskPriority::High,
                                                         d->m_documentHolder->id());
    if (!addingResult) {
        delete task;
        return {};
    }

    d->m_words.reportStarted();
    return d->m_words.future();
}

QFuture<bool> PdfPage::addAnnotation(const QRectF &rect, const QColor &color, const QString &author,
                                     const QString &content)
{
    if (!d->m_documentHolder || !d->m_page)
        return {};

    QFutureInterface<bool> interface;
    const auto &future = interface.future();

    PdfPageAddAnnotationTask::NewAnnotation newAnnotation{ rect, color, author, content };

    auto *task =
            new PdfPageAddAnnotationTask(d->m_page, newAnnotation, interface, d->m_documentHolder);
    auto addingResult = PdfTaskQueue::instance().addTask(task, PdfTaskQueue::TaskPriority::High,
                                                         d->m_documentHolder->id());
    if (!addingResult) {
        delete task;
        return {};
    }
    interface.reportStarted();
    return future;
}

QFuture<bool> PdfPage::addSearchResultAnnotation(const QRectF &rect, const QString &content)
{
    return addAnnotation(rect, QColor(Qt::GlobalColor::green), "TEXT_SEARCH", content);
}

QFuture<bool> PdfPage::addAnnotationPathLayer(const QRectF &rect, const QColor &color,
                                              const QList<QList<QPointF>> &points,
                                              const int lineWidth, const QString &author,
                                              const QString &content, const int annotationType)
{
    if (!d->m_documentHolder || !d->m_page)
        return {};

    QFutureInterface<bool> interface;
    const auto &future = interface.future();

    if (annotationType == Highlight) {
        PdfPageAddAnnotationTask::NewAnnotation newAnnotation{ rect, color, author, content };

        auto *task = new PdfPageAddAnnotationTask(d->m_page, newAnnotation, interface,
                                                  d->m_documentHolder);
        auto addingResult = PdfTaskQueue::instance().addTask(task, PdfTaskQueue::TaskPriority::High,
                                                             d->m_documentHolder->id());
        if (!addingResult) {
            delete task;
            return {};
        }
    } else if (annotationType == Ink) {
        PdfPageDrawSolidPaths::Strokes strokes{ rect, color, points, lineWidth };

        auto *task = new PdfPageDrawSolidPaths(d->m_page, strokes, interface, d->m_documentHolder);
        auto addingResult = PdfTaskQueue::instance().addTask(task, PdfTaskQueue::TaskPriority::High,
                                                             d->m_documentHolder->id());
        if (!addingResult) {
            delete task;
            return {};
        }
    }

    interface.reportStarted();
    return future;
}

QFuture<bool> PdfPage::removeAnnotation(int annotationIndex)
{
    if (!d->m_documentHolder || !d->m_page)
        return {};

    QFutureInterface<bool> interface;
    const auto &future = interface.future();

    auto *task = new PdfPageRemoveAnnotationTask(d->m_page, annotationIndex, interface,
                                                 d->m_documentHolder);
    auto addingResult = PdfTaskQueue::instance().addTask(task, PdfTaskQueue::TaskPriority::High,
                                                         d->m_documentHolder->id());
    if (!addingResult) {
        delete task;
        return {};
    }

    interface.reportStarted();
    return future;
}

QFuture<bool> PdfPage::removeTextSearchResult()
{
    if (!d->m_documentHolder || !d->m_page)
        return {};

    QFutureInterface<bool> interface;
    const auto &future = interface.future();

    auto *task = new PdfPageRemoveSearchResultAnnotationsTask(d->m_page, interface,
                                                 d->m_documentHolder);
    auto addingResult = PdfTaskQueue::instance().addTask(task, PdfTaskQueue::TaskPriority::High,
                                                         d->m_documentHolder->id());
    if (!addingResult) {
        delete task;
        return {};
    }

    interface.reportStarted();
    return future;
}

QFuture<bool> PdfPage::addImage(const QPointF &topLeft, const qreal &pageRate,
                                const QString &imagePath)
{
    if (!d->m_documentHolder || !d->m_page)
        return {};

    QFutureInterface<bool> interface;
    const auto &future = interface.future();

    auto *task = new PdfPageAddImageTask(d->m_page, imagePath, topLeft, pageRate, interface,
                                         d->m_documentHolder);
    auto addingResult = PdfTaskQueue::instance().addTask(task, PdfTaskQueue::TaskPriority::High,
                                                         d->m_documentHolder->id());
    if (!addingResult) {
        delete task;
        return {};
    }

    interface.reportStarted();
    return future;
}
