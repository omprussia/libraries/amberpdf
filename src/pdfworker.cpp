/****************************************************************************
**
** Copyright (C) 2021 - 2023 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the AmberPDF project.
**
** $QT_BEGIN_LICENSE:BSD$
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform LLC copyright holder nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QTimer>

#include "pdftaskqueue.h"
#include "tasks/pdftask.h"
#include "pdfworker.h"

#define TASK_CLEAR_TIME 5000

PdfWorker::PdfWorker(QObject *parent) : QObject(parent)
{
    m_timer = new QTimer(this);
    m_timer->setInterval(1);
    m_timer->setSingleShot(true);

    m_clearTimer = new QTimer(this);
    m_clearTimer->setInterval(TASK_CLEAR_TIME);
    m_clearTimer->setSingleShot(true);

    connect(m_timer, &QTimer::timeout, this, &PdfWorker::_runTask);
    connect(m_clearTimer, &QTimer::timeout, this, &PdfWorker::_clear);
}

void PdfWorker::work()
{
    m_timer->start();
}

void PdfWorker::_runTask()
{
    auto *task = PdfTaskQueue::instance().getTask();
    if (task != nullptr) {
        task->run();
        m_tasksToDelete.insert(task);
    }

    m_timer->start();
    if (!m_clearTimer->isActive())
        m_clearTimer->start();
}

void PdfWorker::_clear()
{
    for (auto task : m_tasksToDelete) {
        if (task != nullptr)
            delete task;
    }

    m_tasksToDelete.clear();
}
