/****************************************************************************
**
** Copyright (C) 2021 - 2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the AmberPDF project.
**
** $QT_BEGIN_LICENSE:BSD$
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform LLC copyright holder nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef PDFANNOTATION_H
#define PDFANNOTATION_H

#include <QObject>
#include <QString>
#include <QVariantMap>
#include <QRectF>
#include <QPointF>
#include <QColor>

class PdfAnnotationPrivate;
class PdfAnnotation : public QObject
{
    Q_OBJECT

    Q_PROPERTY(AnnotationType type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(int linkToPage READ linkToPage WRITE setLinkToPage NOTIFY linkToPageChanged)
    Q_PROPERTY(QString uri READ uri WRITE setUri NOTIFY uriChanged)
    Q_PROPERTY(QRectF rect READ rect WRITE setRect NOTIFY rectChanged)
    Q_PROPERTY(
            QPointF linkPosition READ linkPosition WRITE setLinkPosition NOTIFY linkPositionChanged)
    Q_PROPERTY(QPointF linkCoordinates READ linkCoordinates WRITE setLinkCoordinates NOTIFY
                       linkCoordinatesChanged)
    Q_PROPERTY(QVariantMap values READ values WRITE setValues NOTIFY valuesChanged)
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(QColor interiorColor READ interiorColor WRITE setInteriorColor NOTIFY
                       interiorColorChanged)
    Q_PROPERTY(QList<QPair<QColor, QColor>> objectsColors READ objectsColors WRITE setObjectsColors
                       NOTIFY objectsColorsChanged)
    Q_PROPERTY(QList<QList<QPointF>> points READ points WRITE setPoints NOTIFY pointsChanged)

public:

    static const QString TEXT_SEARCH_AUTHOR_NAME;
    // NOTE key constants
    // https://pdfium.googlesource.com/pdfium/+/master/constants/annotation_common.h
    enum AnnotationKeys {
        Type,
        Subtype,
        Rect,
        Contents,
        P,
        NM,
        M,
        F,
        AP,
        AS,
        Border,
        C,
        StructParent,
        OC,
        Vertices,
        InkList,
        L,
        T
    };
    Q_ENUM(AnnotationKeys)

    // NOTE type constants https://pdfium.googlesource.com/pdfium/+/master/public/fpdf_annot.h#22
    enum AnnotationType {
        Unknown,
        Text,
        Link,
        Freetext,
        Line,
        Square,
        Circle,
        Polygon,
        Polyline,
        Highlight,
        Underline,
        Squiggly,
        Strikeout,
        Stamp,
        Caret,
        Ink,
        Popup,
        Fileattachment,
        Sound,
        Movie,
        Widget,
        Screen,
        Printermark,
        Trapnet,
        Watermark,
        Threed,
        Richmedia,
        Xfawidget,
        Redact
    };
    Q_ENUM(AnnotationType)

    explicit PdfAnnotation(QObject *parent = nullptr);
    ~PdfAnnotation();

    AnnotationType type() const;
    int linkToPage() const;
    QString uri() const;
    QRectF rect() const;
    QPointF linkCoordinates() const;
    QPointF linkPosition() const;
    QVariantMap values() const;
    QColor color() const;
    QColor interiorColor() const;
    QList<QPair<QColor, QColor>> objectsColors() const;
    QList<QList<QPointF>> points() const;
    void setType(AnnotationType type);
    void setLinkToPage(int linkToPage);
    void setUri(const QString &uri);
    void setRect(const QRectF &rect);
    void setLinkCoordinates(const QPointF &linkCoordinates);
    void setLinkPosition(const QPointF &linkPosition);
    void setValues(const QVariantMap &values);
    void setColor(QColor color);
    void setInteriorColor(QColor interiorColor);
    void setObjectsColors(QList<QPair<QColor, QColor>> objectsColors);
    void setPoints(const QList<QList<QPointF>> &points);
    static QString annotationKeyToQString(AnnotationKeys key);

    bool isSearchResult();
signals:
    void typeChanged(AnnotationType type);
    void linkToPageChanged(int linkToPage);
    void uriChanged(QString uri);
    void rectChanged(QRectF rect);
    void linkPositionChanged(QPointF linkPosition);
    void linkCoordinatesChanged(QPointF linkCoordinates);
    void valuesChanged(QVariantMap values);
    void colorChanged(QColor color);
    void interiorColorChanged(QColor interiorColor);
    void objectsColorsChanged(QList<QPair<QColor, QColor>> objectsColors);
    void pointsChanged(QList<QList<QPointF>> points);

private:
    Q_DISABLE_COPY(PdfAnnotation);
    Q_DECLARE_PRIVATE(PdfAnnotation)

    PdfAnnotationPrivate *const d_ptr;
};

Q_DECLARE_METATYPE(PdfAnnotation *)

#endif // PDFANNOTATION_H
