/****************************************************************************
**
** Copyright (C) 2021 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the AmberPDF project.
**
** $QT_BEGIN_LICENSE:BSD$
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform LLC copyright holder nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "tasks/pdfdocumentclosetask.h"
#include "pdftaskqueue.h"

#include "pdfdocumentholder.h"

static int getId()
{
    static QAtomicInt id = -1;
    return ++id;
}

PdfDocumentHolder::PdfDocumentHolder(QSharedPointer<fpdf_document_t__> doc) : m_document(doc)
{
    m_id = getId();
}

PdfDocumentHolder::~PdfDocumentHolder()
{
    auto closeDocumentInterface = QFutureInterface<void>();
    auto *closeDocumentTask = new PdfDocumentCloseTask(m_document, closeDocumentInterface);
    PdfTaskQueue::instance().addTask(closeDocumentTask);
    closeDocumentInterface.reportStarted();
    for (FILE *file : m_pdfImages)
        fclose(file);
}

QSharedPointer<fpdf_document_t__> PdfDocumentHolder::document() const
{
    return m_document;
}

QList<FILE *> PdfDocumentHolder::pdfImages() const
{
    return m_pdfImages;
}

int PdfDocumentHolder::id() const
{
    return m_id;
}

void PdfDocumentHolder::setPdfImagePath(QString imagePath)
{
    m_pdfImages.append(fopen(imagePath.toStdString().data(), "rb"));
}
