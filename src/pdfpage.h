/****************************************************************************
**
** Copyright (C) 2021 - 2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the AmberPDF project.
**
** $QT_BEGIN_LICENSE:BSD$
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform LLC copyright holder nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef PDFPAGE_H
#define PDFPAGE_H

#include <QObject>
#include <QSharedDataPointer>
#include <QImage>
#include <QPointF>
#include <QList>
#include <QFuture>

class PdfWord;
class PdfPageData;
class PdfPage
{
public:
    // NOTE: values https://pdfium.googlesource.com/pdfium/+/refs/heads/master/public/fpdfview.h#762
    enum RenderFlags {
        NoFlags = 0,
        // Set if annotations are to be rendered.
        Annot = 0x01,
        // Set if using text rendering optimized for LCD display.
        LCDText = 0x02,
        // Grayscale output.
        GrayScale = 0x08,
        // Limit image cache size.
        LimitImageCache = 0x200,
        // Always use halftone for image stretching.
        ForceHalfTone = 0x400,
        // Render for printing.
        Printing = 0x800,
        // Set to disable anti-aliasing on text.
        NoSmoothText = 0x1000,
        // Set to disable anti-aliasing on images.
        NoSmoothing = 0x2000,
        // Set to disable anti-aliasing on paths.
        NoSmoothPath = 0x4000,
        // Set whether to render in a reverse Byte order, this flag is only used when
        // rendering to a bitmap.
        ReverseByteOrder = 0x10
    };

    enum AnnotationType { Highlight, Ink };

    PdfPage();
    virtual ~PdfPage();

    int pageNumber() const;
    bool isValid() const;

    QFuture<QSizeF> originalSize();
    QFuture<QImage> bitmapFull(qreal pageScale, int renderFlags = 0) const;
    QFuture<QImage> bitmapPart(qreal pageScaleX, qreal pageScaleY, int renderFlags = 0,
                               qreal zoom = 1.0, const QPointF &bias = QPointF()) const;
    QFuture<QList<QObject *>> annotations();
    QFuture<QList<QObject *>> words();
    QFuture<bool> addAnnotation(const QRectF &rect, const QColor &color,
                                const QString &author = QStringLiteral(""),
                                const QString &content = QStringLiteral(""));
    QFuture<bool> addSearchResultAnnotation(const QRectF &rect, const QString &content = QStringLiteral(""));
    QFuture<bool> addAnnotationPathLayer(const QRectF &rect, const QColor &color,
                                         const QList<QList<QPointF>> &points, const int lineWidth,
                                         const QString &author = QStringLiteral(""),
                                         const QString &content = QStringLiteral(""),
                                         const int annotationType = 0);
    QFuture<bool> removeAnnotation(int annotationIndex);
    QFuture<bool> removeTextSearchResult();
    QFuture<bool> addImage(const QPointF &topLeft, const qreal &pageRate, const QString &imagePath);

private:
    QSharedDataPointer<PdfPageData> d;

    friend class PdfPageLoadTask;
};

Q_DECLARE_METATYPE(PdfPage::RenderFlags)
Q_DECLARE_METATYPE(QFuture<QList<QObject *>>);

#endif // PDFPAGE_H
