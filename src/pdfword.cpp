/****************************************************************************
**
** Copyright (C) 2021 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the AmberPDF project.
**
** $QT_BEGIN_LICENSE:BSD$
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform LLC copyright holder nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "pdfword.h"

class PdfWordPrivate
{
public:
    int m_index{-1};
    QRectF m_rect;
    QString m_value;
};

PdfWord::PdfWord(QObject *parent) : QObject(parent),
    d_ptr(new PdfWordPrivate())
{ }

PdfWord::~PdfWord() = default;

int PdfWord::index() const
{
    Q_D(const PdfWord);
    return d->m_index;
}

QRectF PdfWord::rect() const
{
    Q_D(const PdfWord);
    return d->m_rect;
}

QString PdfWord::value() const
{
    Q_D(const PdfWord);
    return d->m_value;
}

void PdfWord::setIndex(int index)
{
    Q_D(PdfWord);
    if (d->m_index == index)
        return;

    d->m_index = index;
    emit indexChanged(d->m_index);
}

void PdfWord::setRect(const QRectF &rect)
{
    Q_D(PdfWord);
    if (d->m_rect == rect)
        return;

    d->m_rect = rect;
    emit rectChanged(d->m_rect);
}

void PdfWord::setValue(const QString &value)
{
    Q_D(PdfWord);
    if (d->m_value == value)
        return;

    d->m_value = value;
    emit valueChanged(d->m_value);
}
