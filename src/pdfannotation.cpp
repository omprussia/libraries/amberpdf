/****************************************************************************
**
** Copyright (C) 2021 - 2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the AmberPDF project.
**
** $QT_BEGIN_LICENSE:BSD$
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform LLC copyright holder nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QMetaEnum>

#include "pdfannotation.h"

class PdfAnnotationPrivate
{
public:
    int m_linkToPage{ -1 };
    PdfAnnotation::AnnotationType m_type{ PdfAnnotation::AnnotationType::Unknown };
    QRectF m_rect;
    QVariantMap m_values;
    QPointF m_linkCoordinates;
    QPointF m_linkPosition;
    QString m_uri;
    QColor m_color;
    QColor m_interiorColor;
    QList<QPair<QColor, QColor>> m_objectsColors;
    QList<QList<QPointF>> m_points;
};

const QString PdfAnnotation::TEXT_SEARCH_AUTHOR_NAME = "TEXT_SEARCH";

PdfAnnotation::PdfAnnotation(QObject *parent) : QObject(parent), d_ptr(new PdfAnnotationPrivate())
{
}

PdfAnnotation::~PdfAnnotation() = default;

PdfAnnotation::AnnotationType PdfAnnotation::type() const
{
    Q_D(const PdfAnnotation);
    return d->m_type;
}

bool PdfAnnotation::isSearchResult()
{
    return this->values().value("T").toString() == PdfAnnotation::TEXT_SEARCH_AUTHOR_NAME;
}

void PdfAnnotation::setLinkToPage(int linkToPage)
{
    Q_D(PdfAnnotation);
    if (d->m_linkToPage == linkToPage)
        return;

    d->m_linkToPage = linkToPage;
    emit linkToPageChanged(d->m_linkToPage);
}

QString PdfAnnotation::uri() const
{
    Q_D(const PdfAnnotation);
    return d->m_uri;
}

QRectF PdfAnnotation::rect() const
{
    Q_D(const PdfAnnotation);
    return d->m_rect;
}

QPointF PdfAnnotation::linkCoordinates() const
{
    Q_D(const PdfAnnotation);
    return d->m_linkCoordinates;
}

QPointF PdfAnnotation::linkPosition() const
{
    Q_D(const PdfAnnotation);
    return d->m_linkPosition;
}

QVariantMap PdfAnnotation::values() const
{
    Q_D(const PdfAnnotation);
    return d->m_values;
}

int PdfAnnotation::linkToPage() const
{
    Q_D(const PdfAnnotation);
    return d->m_linkToPage;
}

void PdfAnnotation::setType(PdfAnnotation::AnnotationType type)
{
    Q_D(PdfAnnotation);
    if (d->m_type == type)
        return;

    d->m_type = type;
    emit typeChanged(d->m_type);
}

void PdfAnnotation::setUri(const QString &uri)
{
    Q_D(PdfAnnotation);
    if (d->m_uri == uri)
        return;

    d->m_uri = uri;
    emit uriChanged(d->m_uri);
}

void PdfAnnotation::setRect(const QRectF &rect)
{
    Q_D(PdfAnnotation);
    if (d->m_rect == rect)
        return;

    d->m_rect = rect;
    emit rectChanged(d->m_rect);
}

void PdfAnnotation::setLinkCoordinates(const QPointF &linkCoordinates)
{
    Q_D(PdfAnnotation);
    if (d->m_linkCoordinates == linkCoordinates)
        return;

    d->m_linkCoordinates = linkCoordinates;
}

void PdfAnnotation::setLinkPosition(const QPointF &linkPosition)
{
    Q_D(PdfAnnotation);
    if (d->m_linkPosition == linkPosition)
        return;

    d->m_linkPosition = linkPosition;
}

void PdfAnnotation::setValues(const QVariantMap &values)
{
    Q_D(PdfAnnotation);
    if (d->m_values == values)
        return;

    d->m_values = values;
    emit valuesChanged(d->m_values);
}

QString PdfAnnotation::annotationKeyToQString(PdfAnnotation::AnnotationKeys key)
{
    static auto metaEnum = QMetaEnum::fromType<PdfAnnotation::AnnotationKeys>();
    return QString(metaEnum.valueToKey(static_cast<int>(key)));
}

void PdfAnnotation::setObjectsColors(QList<QPair<QColor, QColor>> objectsColors)
{
    Q_D(PdfAnnotation);
    if (d->m_objectsColors == objectsColors)
        return;

    d->m_objectsColors = objectsColors;
    emit objectsColorsChanged(d->m_objectsColors);
}

QList<QPair<QColor, QColor>> PdfAnnotation::objectsColors() const
{
    Q_D(const PdfAnnotation);
    return d->m_objectsColors;
}

void PdfAnnotation::setPoints(const QList<QList<QPointF>> &points)
{
    Q_D(PdfAnnotation);
    if (d->m_points == points)
        return;

    d->m_points = points;
    emit pointsChanged(d->m_points);
}

QList<QList<QPointF>> PdfAnnotation::points() const
{
    Q_D(const PdfAnnotation);
    return d->m_points;
}

QColor PdfAnnotation::interiorColor() const
{
    Q_D(const PdfAnnotation);
    return d->m_interiorColor;
}

void PdfAnnotation::setInteriorColor(QColor interiorColor)
{
    Q_D(PdfAnnotation);
    if (d->m_interiorColor == interiorColor)
        return;

    d->m_interiorColor = interiorColor;
    emit interiorColorChanged(d->m_interiorColor);
}

QColor PdfAnnotation::color() const
{
    Q_D(const PdfAnnotation);
    return d->m_color;
}

void PdfAnnotation::setColor(QColor color)
{
    Q_D(PdfAnnotation);
    if (d->m_color == color)
        return;

    d->m_color = color;
    emit colorChanged(d->m_color);
}
