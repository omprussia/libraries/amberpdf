/****************************************************************************
**
** Copyright (C) 2021 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the AmberPDF project.
**
** $QT_BEGIN_LICENSE:BSD$
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform LLC copyright holder nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef PDFDOCUMENT_P_H
#define PDFDOCUMENT_P_H

#include <QSharedPointer>
#include <QFuture>
#include <QMap>
#include <QFutureWatcher>

class PdfDocumentHolder;
struct fpdf_document_t__;
class PdfPage;
struct PdfDocumentPrivate
{
    void clear();

    static QFuture<QPair<int, QSharedPointer<fpdf_document_t__>>> fromPdf(const QString &fileName, const QString &password);
    static QFuture<QPair<int, QSharedPointer<fpdf_document_t__>>> fromMem(const QByteArray &buffer, const QString &password);
    void handleDocument(QPair<int, QSharedPointer<fpdf_document_t__>> loadResult);

    PdfDocument::DocumentStatus m_status;
    int m_pagesCount{ -1 };
    QString m_path;
    QSharedPointer<PdfDocumentHolder> m_documentHolder;
    QFutureWatcher<int> m_pageCountWatcher;
    mutable QMap<int, QFuture<QSharedPointer<PdfPage>>> m_openPages;
    QFutureWatcher<int> m_versionWatcher;
    int m_fileVersion{ -1 };
};

#endif // PDFDOCUMENT_P_H
