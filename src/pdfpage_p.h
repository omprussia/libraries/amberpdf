/****************************************************************************
**
** Copyright (C) 2021 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the AmberPDF project.
**
** $QT_BEGIN_LICENSE:BSD$
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform LLC copyright holder nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef PDFPAGE_P_H
#define PDFPAGE_P_H

#include <QSharedPointer>
#include <QSharedData>
#include <QSizeF>
#include <QFutureInterface>

struct fpdf_page_t__;
struct fpdf_textpage_t__;
class PdfDocumentHolder;
class PdfPageData : public QSharedData
{
public:
    PdfPageData();
    PdfPageData(int pageNumber, QSharedPointer<fpdf_page_t__> page, QSharedPointer<PdfDocumentHolder> doc);
    ~PdfPageData();
    PdfPageData(const PdfPageData &other);
    PdfPageData(PdfPageData &&other);
    PdfPageData &operator=(const PdfPageData &other);
    PdfPageData &operator=(PdfPageData &&other);

    int m_pageNumber;
    QSharedPointer<fpdf_page_t__> m_page;
    mutable QSharedPointer<PdfDocumentHolder> m_documentHolder;
    QSharedPointer<fpdf_textpage_t__> m_textPage;
    QFutureInterface<QList<QObject *>> m_words;
    mutable QFutureInterface<QSizeF> m_originalSize;
};

#endif // PDFPAGE_P_H
