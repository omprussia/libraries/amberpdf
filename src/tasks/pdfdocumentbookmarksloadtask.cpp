/****************************************************************************
**
** Copyright (C) 2022 - 2023 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the AmberPDF project.
**
** $QT_BEGIN_LICENSE:BSD$
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform LLC copyright holder nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <pdfium/fpdfview.h>
#include <pdfium/fpdf_doc.h>

#include "pdfdocumentholder.h"
#include "pdftaskqueue.h"
#include "pdfdocumentbookmarksloadtask.h"

static  void parseBookmarks(QVector<PdfBookmark> *bookmarks, fpdf_document_t__ *document, FPDF_BOOKMARK current, int level) {
    if (document == nullptr)
        return;

    if (current == nullptr) {
        auto firstBookmark = FPDFBookmark_GetFirstChild(document, nullptr);
        if (firstBookmark == nullptr)
            return;
    }

    auto titleLen = FPDFBookmark_GetTitle(current, nullptr, 0);
    QVector<unsigned short> buffer(titleLen);
    FPDFBookmark_GetTitle(current, buffer.data(), titleLen);
    auto title = QString::fromUtf16(buffer.data(), titleLen / sizeof(unsigned short) - 1);

    auto dest = FPDFBookmark_GetDest(document, current);
    FS_FLOAT x = 0.0;
    FS_FLOAT y = 0.0;
    FS_FLOAT z = 0.0;
    FPDF_BOOL hasX = false;
    FPDF_BOOL hasY = false;
    FPDF_BOOL hasZ = false;
    FPDFDest_GetLocationInPage(dest, &hasX, &hasY, &hasZ, &x, &y, &z);
    auto pageIndex = FPDFDest_GetDestPageIndex(document, dest);
    if (titleLen > 0 && pageIndex > 0)
        bookmarks->push_back({ title, pageIndex, level, {x, y}});

    auto child = FPDFBookmark_GetFirstChild(document, current);
    if (child != nullptr) {
        parseBookmarks(bookmarks, document, child, ++level);
        --level;
    }

    auto sibling = FPDFBookmark_GetNextSibling(document, current);
    if (sibling != nullptr)
        parseBookmarks(bookmarks, document, sibling, level);
}

PdfDocumentBookmarksLoadTask::PdfDocumentBookmarksLoadTask(
        const QSharedPointer<PdfDocumentHolder> &documentHolder,
        const QFutureInterface<QVector<PdfBookmark> > &interface) :
    m_documentHolder(documentHolder),
    m_interface(interface)
{}

void PdfDocumentBookmarksLoadTask::run()
{
    if (m_interface.isCanceled() || PdfTaskQueue::instance().blockedId().contains(m_documentHolder->id()))
        return;

    parseBookmarks(&m_bookmarks, m_documentHolder->document().data(), nullptr, 0);
    m_interface.reportFinished(&m_bookmarks);
}

void PdfDocumentBookmarksLoadTask::cancel() {}

int PdfDocumentBookmarksLoadTask::id() const
{
    return m_documentHolder->id();
}
