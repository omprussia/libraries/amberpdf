/****************************************************************************
**
** Copyright (C) 2021 - 2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the AmberPDF project.
**
** $QT_BEGIN_LICENSE:BSD$
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform LLC copyright holder nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <pdfium/fpdfview.h>
#include <pdfium/fpdf_annot.h>
#include <pdfium/fpdf_edit.h>

#include "pdfdocumentholder.h"

#include "pdfpagedrawsolidpaths.h"

PdfPageDrawSolidPaths::PdfPageDrawSolidPaths(const QSharedPointer<fpdf_page_t__> &page,
                                             const Strokes &strokes,
                                             const QFutureInterface<bool> &interface,
                                             const QSharedPointer<PdfDocumentHolder> &document)
    : m_page(page), m_strokes(strokes), m_interface(interface), m_documentHolder(document)
{
}

PdfPageDrawSolidPaths::~PdfPageDrawSolidPaths() = default;

void PdfPageDrawSolidPaths::run()
{
    if (!m_documentHolder)
        return;

    if (m_interface.isCanceled())
        return;

    auto result = false;

    for (QList<QPointF> &stroke : m_strokes.points) {
        if (!stroke.isEmpty()) {
            QPointF start = stroke.takeFirst();
            FPDF_PAGEOBJECT path = FPDFPageObj_CreateNewPath(start.x(), start.y());
            if (!path) {
                m_interface.reportFinished(&result);
                return;
            }

            result = FPDFPageObj_SetStrokeWidth(path, m_strokes.lineWidth);
            if (!result) {
                m_interface.reportFinished(&result);
                return;
            }

            result =
                    FPDFPageObj_SetStrokeColor(path, m_strokes.color.red(), m_strokes.color.green(),
                                               m_strokes.color.blue(), m_strokes.color.alpha());
            if (!result) {
                m_interface.reportFinished(&result);
                return;
            }

            result = FPDFPath_SetDrawMode(path, FPDF_FILLMODE_NONE, true);
            if (!result) {
                m_interface.reportFinished(&result);
                return;
            }

            for (QPointF &point : stroke) {
                result = FPDFPath_LineTo(path, point.x(), point.y());
                if (!result) {
                    m_interface.reportFinished(&result);
                    return;
                }
            }

            FPDFPage_InsertObject(m_page.data(), path);
        }
    }

    result = FPDFPage_GenerateContent(m_page.data());

    m_interface.reportFinished(&result);
}

void PdfPageDrawSolidPaths::cancel() { }

int PdfPageDrawSolidPaths::id() const
{
    return m_documentHolder->id();
}
