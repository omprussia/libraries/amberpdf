/****************************************************************************
**
** Copyright (C) 2021 - 2023 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the AmberPDF project.
**
** $QT_BEGIN_LICENSE:BSD$
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform LLC copyright holder nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <pdfium/fpdfview.h>

#include "pdftaskqueue.h"
#include "pdfdocumentholder.h"
#include "pdfpage.h"
#include "pdfpage_p.h"
#include "pdfpageloadtask.h"

PdfPageLoadTask::PdfPageLoadTask(const QSharedPointer<PdfDocumentHolder> &document, const QFutureInterface<QSharedPointer<PdfPage> > &interface, int pageNumber) :
    m_pageNumber(pageNumber),
    m_interface(interface),
    m_documentHolder(document)
{  }

PdfPageLoadTask::~PdfPageLoadTask() = default;

void PdfPageLoadTask::run()
{
    if (m_interface.isCanceled() || PdfTaskQueue::instance().blockedId().contains(m_documentHolder->id()))
        return;

    auto pdfiumPage = QSharedPointer<fpdf_page_t__>(FPDF_LoadPage(m_documentHolder->document().data(), m_pageNumber), [](fpdf_page_t__ *){  });
    m_page.reset(new PdfPage());
    m_page->d->m_page = pdfiumPage;
    m_page->d->m_documentHolder = m_documentHolder;
    m_page->d->m_pageNumber = m_pageNumber;
    m_interface.reportFinished(&m_page);
}

int PdfPageLoadTask::id() const
{
    return m_documentHolder->id();
}

void PdfPageLoadTask::cancel() {  }
