/****************************************************************************
**
** Copyright (C) 2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the AmberPDF project.
**
** $QT_BEGIN_LICENSE:BSD$
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform LLC copyright holder nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <pdfium/fpdfview.h>
#include <pdfium/fpdf_edit.h>
#include <QImage>
#include <QFile>

#include "pdfdocumentholder.h"
#include "pdfpageaddimagetask.h"

PdfPageAddImageTask::PdfPageAddImageTask(const QSharedPointer<fpdf_page_t__> &page,
                                         const QString &imagePath, const QPointF &imageTopLeft,
                                         const qreal &pageRate,
                                         const QFutureInterface<bool> &interface,
                                         const QSharedPointer<PdfDocumentHolder> &document)
    : m_page(page),
      m_imagePath(imagePath),
      m_imageTopLeft(imageTopLeft),
      m_pageRate(pageRate),
      m_interface(interface),
      m_documentHolder(document)
{
}

void PdfPageAddImageTask::run()
{
    if (!m_documentHolder)
        return;

    if (m_interface.isCanceled())
        return;

    bool result = false;

    QImage image(m_imagePath);
    image = image.convertToFormat(QImage::Format_RGBA8888);
    image = image.rgbSwapped();
    FPDF_BITMAP bitmap = FPDFBitmap_CreateEx(image.width(), image.height(), FPDFBitmap_BGRA,
                                             image.scanLine(0), image.bytesPerLine());
    FPDF_PAGEOBJECT page_obj_image = FPDFPageObj_NewImageObj(m_documentHolder->document().data());
    result = FPDFImageObj_SetBitmap(nullptr, 0, page_obj_image, bitmap);
    if (!result) {
        m_interface.reportFinished(&result);
        return;
    }

    FPDFPageObj_Transform(page_obj_image, image.width() / m_pageRate, 0, 0,
                          image.height() / m_pageRate, m_imageTopLeft.x(),
                          m_imageTopLeft.y() - image.height() / m_pageRate);
    FPDFPage_InsertObject(m_page.data(), page_obj_image);
    result = FPDFPage_GenerateContent(m_page.data());
    m_interface.reportFinished(&result);
}

void PdfPageAddImageTask::cancel() { }

int PdfPageAddImageTask::id() const
{
    return m_documentHolder->id();
}
