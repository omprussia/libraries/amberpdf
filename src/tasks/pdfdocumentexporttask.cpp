/****************************************************************************
**
** Copyright (C) 2023 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the Tiny PDF Viewer project.
**
** $QT_BEGIN_LICENSE:BSD$
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform LLC copyright holder nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "pdfdocumentexporttask.h"

#include <pdfium/fpdf_save.h>

struct Writer : public FPDF_FILEWRITE
{
    QIODevice *output;
};

PdfDocumentExportTask::PdfDocumentExportTask(QIODevice *output, QFutureInterface<bool> interface,
                                             QSharedPointer<fpdf_document_t__> document)
    : m_output(output), m_interface(interface), m_document(document)
{
}

void PdfDocumentExportTask::run()
{
    bool result = false;
    if (!m_document) {
        m_interface.reportFinished(&result);
        return;
    }

    auto writeLambda = [](FPDF_FILEWRITE *fw, const void *data, unsigned long size) -> int {
        Writer *writer = reinterpret_cast<Writer *>(fw);
        auto bytesWrites = writer->output->write(reinterpret_cast<const char *>(data), size);
        return int(bytesWrites);
    };

    Writer filewrite;
    filewrite.version = 1;
    filewrite.WriteBlock = writeLambda;
    filewrite.output = m_output;

    result = m_output->open(QIODevice::WriteOnly);
    if (!result) {
        m_interface.reportFinished(&result);
        return;
    }

    result = FPDF_SaveAsCopy(m_document.data(), &filewrite, 0);
    m_output->close();
    m_interface.reportFinished(&result);
}

void PdfDocumentExportTask::cancel() { }

int PdfDocumentExportTask::id() const
{
    return 0;
}
