// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <pdfannotation.h>
#include "pdfdocumentholder.h"
#include "pdfpageremovesearchresultannotationstask.h"

PdfPageRemoveSearchResultAnnotationsTask::PdfPageRemoveSearchResultAnnotationsTask(const QSharedPointer<fpdf_page_t__> &page,
                                                         const QFutureInterface<bool> &interface,
                                                         const QSharedPointer<PdfDocumentHolder> &document) :
    m_page(page),
    m_interface(interface),
    m_documentHolder(document)
{}

QVariant PdfPageRemoveSearchResultAnnotationsTask::getAnnotationValue(FPDF_ANNOTATION currentAnnot, QString key)
{
    auto length = FPDFAnnot_GetStringValue(currentAnnot, qUtf8Printable(key), nullptr, 0);
    QVector<ushort> buffer(static_cast<qint32>(length));
    FPDFAnnot_GetStringValue(currentAnnot, qUtf8Printable(key), buffer.data(),
                             static_cast<quint32>(buffer.length()));
    auto annotString = QString::fromUtf16(buffer.data());
    return QVariant::fromValue(annotString);
}

void PdfPageRemoveSearchResultAnnotationsTask::run()
{
    if (!m_documentHolder)
        return;

    if (m_interface.isCanceled())
        return;

    bool result = false;
    auto annotationsCount = FPDFPage_GetAnnotCount(m_page.data());

    for (int i = annotationsCount - 1; i >= 0; i--)
    {
        auto currentAnnot = FPDFPage_GetAnnot(m_page.data(), i);

        auto value = getAnnotationValue(currentAnnot, PdfAnnotation::annotationKeyToQString(PdfAnnotation::AnnotationKeys::T));
        if (value.toString() == PdfAnnotation::TEXT_SEARCH_AUTHOR_NAME) {
            result &= FPDFPage_RemoveAnnot(m_page.data(), FPDFPage_GetAnnotIndex(m_page.data(), currentAnnot));
        }
        FPDFPage_CloseAnnot(currentAnnot);
    }

    m_interface.reportFinished(&result);

}

void PdfPageRemoveSearchResultAnnotationsTask::cancel() {  }

int PdfPageRemoveSearchResultAnnotationsTask::id() const
{
    return m_documentHolder->id();
}
