/****************************************************************************
**
** Copyright (C) 2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the AmberPDF project.
**
** $QT_BEGIN_LICENSE:BSD$
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform LLC copyright holder nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <pdfium/fpdfview.h>
#include <pdfium/fpdf_annot.h>

#include "pdfdocumentholder.h"

#include "pdfpageaddannotationtask.h"

PdfPageAddAnnotationTask::PdfPageAddAnnotationTask(
        const QSharedPointer<fpdf_page_t__> &page, const NewAnnotation &newAnnotation,
        const QFutureInterface<bool> &interface, const QSharedPointer<PdfDocumentHolder> &document)
    : m_page(page),
      m_newAnnotation(newAnnotation),
      m_interface(interface),
      m_documentHolder(document)
{
}

PdfPageAddAnnotationTask::~PdfPageAddAnnotationTask() = default;

void PdfPageAddAnnotationTask::run()
{
    if (!m_documentHolder)
        return;

    if (m_interface.isCanceled())
        return;

    auto result = false;

    auto annotation = FPDFPage_CreateAnnot(m_page.data(), FPDF_ANNOT_TEXT);
    if (annotation == nullptr) {
        m_interface.reportFinished(&result);
        return;
    }

    FS_RECTF annotationRect;
    annotationRect.bottom = m_newAnnotation.rect.bottom();
    annotationRect.top = m_newAnnotation.rect.top();
    annotationRect.left = m_newAnnotation.rect.left();
    annotationRect.right = m_newAnnotation.rect.right();
    result = FPDFAnnot_SetRect(annotation, &annotationRect);
    if (!result) {
        m_interface.reportFinished(&result);
        return;
    }

    result = FPDFAnnot_SetColor(annotation, FPDFANNOT_COLORTYPE_Color, m_newAnnotation.color.red(),
                                m_newAnnotation.color.green(), m_newAnnotation.color.blue(),
                                m_newAnnotation.color.alpha());
    if (!result) {
        m_interface.reportFinished(&result);
        return;
    }

    result = FPDFAnnot_SetStringValue(annotation, "T", m_newAnnotation.author.utf16());
    if (!result) {
        m_interface.reportFinished(&result);
        return;
    }

    result = FPDFAnnot_SetStringValue(annotation, "Contents", m_newAnnotation.content.utf16());
    if (!result) {
        m_interface.reportFinished(&result);
        return;
    }

    FPDFPage_CloseAnnot(annotation);

    m_interface.reportFinished(&result);
}

void PdfPageAddAnnotationTask::cancel() { }

int PdfPageAddAnnotationTask::id() const
{
    return m_documentHolder->id();
}
