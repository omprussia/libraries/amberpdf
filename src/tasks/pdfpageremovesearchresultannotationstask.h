// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QFuture>
#include <QSharedPointer>
#include <pdfium/fpdfview.h>
#include <pdfium/fpdf_annot.h>

#include "pdftask.h"

struct fpdf_page_t__;
class PdfDocumentHolder;
class PdfPageRemoveSearchResultAnnotationsTask : public PdfTask
{
public:
    PdfPageRemoveSearchResultAnnotationsTask(const QSharedPointer<fpdf_page_t__> &page,
                                const QFutureInterface<bool> &interface,
                                const QSharedPointer<PdfDocumentHolder> &document);
    ~PdfPageRemoveSearchResultAnnotationsTask() = default;

    void run() override;
    void cancel() override;
    int id() const override;

private:
    QSharedPointer<fpdf_page_t__> m_page;
    QFutureInterface<bool> m_interface;
    QSharedPointer<PdfDocumentHolder> m_documentHolder;
    QVariant getAnnotationValue(FPDF_ANNOTATION currentAnnot, QString key);
};
