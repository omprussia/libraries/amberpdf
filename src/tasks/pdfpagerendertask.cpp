/****************************************************************************
**
** Copyright (C) 2021 - 2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the AmberPDF project.
**
** $QT_BEGIN_LICENSE:BSD$
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform LLC copyright holder nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <pdfium/fpdfview.h>

#include "pdftaskqueue.h"
#include "pdfdocumentholder.h"
#include "pdfpagerendertask.h"

PdfPageRenderFullTask::PdfPageRenderFullTask(float pageScale, int renderFlags,
                                     const QSizeF pageSize, const QSharedPointer<fpdf_page_t__> &page,
                                     const QSharedPointer<PdfDocumentHolder> &documentHolder,
                                     const QFutureInterface<QImage> &interface) :
    m_pageScale(pageScale),
    m_renderFlags(renderFlags),
    m_pageSize(pageSize),
    m_page(page),
    m_documentHolder(documentHolder),
    m_interface(interface)
{  }

PdfPageRenderFullTask::~PdfPageRenderFullTask() = default;

void PdfPageRenderFullTask::run()
{
    QImage image = QImage(static_cast<int>(m_pageSize.width() * m_pageScale),
                          static_cast<int>(m_pageSize.height() * m_pageScale),
                          QImage::Format_ARGB32);

    if (image.isNull()) {
        m_interface.reportFinished(&image);
        return;
    }

    if (m_interface.isCanceled() || PdfTaskQueue::instance().blockedId().contains(m_documentHolder->id())) {
        m_interface.reportFinished(&image);
        return;
    }

    image.fill(Qt::white);

    FPDF_BITMAP bitmap = FPDFBitmap_CreateEx(image.width(), image.height(), FPDFBitmap_BGRA, image.bits(), image.bytesPerLine());
    if (bitmap == nullptr) {
        FPDFBitmap_Destroy(bitmap);
        m_interface.reportFinished(&image);
        return;
    }

    FPDF_RenderPageBitmap(bitmap, m_page.data(), 0, 0, image.width(), image.height(), 0, m_renderFlags);

    FPDFBitmap_Destroy(bitmap);

    m_interface.reportFinished(&image);
}

int PdfPageRenderFullTask::id() const
{
    return m_documentHolder->id();
}

void PdfPageRenderFullTask::cancel() {  }


PdfPageRenderPartTask::~PdfPageRenderPartTask() = default;

PdfPageRenderPartTask::PdfPageRenderPartTask(float pageScaleX, float pageScaleY,
                                             int renderFlags, float zoom,
                                             const QPointF &bias, const QSizeF pageSize,
                                             const QSharedPointer<fpdf_page_t__> &page,
                                             const QSharedPointer<PdfDocumentHolder> &documentHolder,
                                             const QFutureInterface<QImage> &interface) :
    m_pageScaleX(pageScaleX),
    m_pageScaleY(pageScaleY),
    m_renderFlags(renderFlags),
    m_zoom(zoom),
    m_bias(bias),
    m_pageSize(pageSize),
    m_page(page),
    m_documentHolder(documentHolder),
    m_interface(interface)
{  }

void PdfPageRenderPartTask::run()
{
    QImage image = QImage(static_cast<int>(m_pageSize.width() * m_pageScaleX),
                          static_cast<int>(m_pageSize.height() * m_pageScaleY),
                          QImage::Format_ARGB32);

    if (image.isNull()) {
        m_interface.reportFinished(&image);
        return;
    }

    if (m_interface.isCanceled() || PdfTaskQueue::instance().blockedId().contains(m_documentHolder->id())) {
        m_interface.reportFinished(&image);
        return;
    }

    image.fill(Qt::white);

    FPDF_BITMAP bitmap = FPDFBitmap_CreateEx(image.width(), image.height(), FPDFBitmap_BGRA, image.bits(), image.bytesPerLine());
    if (bitmap == nullptr) {
        FPDFBitmap_Destroy(bitmap);
        m_interface.reportFinished(&image);
        return;
    }

    FS_MATRIX matrix { static_cast<float>(m_zoom), 0, 0, static_cast<float>(m_zoom), static_cast<float>(m_bias.x()), static_cast<float>(m_bias.y()) };
    FS_RECTF clipRectF { 0, 0, float(image.width()), float(image.height()) };
    FPDF_RenderPageBitmapWithMatrix(bitmap, m_page.data(), &matrix, &clipRectF, m_renderFlags);

    FPDFBitmap_Destroy(bitmap);

    m_interface.reportFinished(&image);
}

void PdfPageRenderPartTask::cancel() {  }

int PdfPageRenderPartTask::id() const
{
    return m_documentHolder->id();
}
