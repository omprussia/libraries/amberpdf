/****************************************************************************
**
** Copyright (C) 2021 - 2023 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the AmberPDF project.
**
** $QT_BEGIN_LICENSE:BSD$
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform LLC copyright holder nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QColor>

#include <pdfium/fpdfview.h>
#include <pdfium/fpdf_annot.h>
#include <pdfium/fpdf_edit.h>
#include <pdfium/fpdf_doc.h>

#include "pdfannotation.h"
#include "pdfdocumentholder.h"
#include "pdftaskqueue.h"

#include "pdfpageannotationloadtask.h"

PdfPageAnnotationTask::PdfPageAnnotationTask(const QSharedPointer<fpdf_page_t__> &page,
                                             const QList<QObject *> &annotations,
                                             const QFutureInterface<QList<QObject *>> &interface,
                                             const QSharedPointer<PdfDocumentHolder> &document)
    : m_page(page), m_annotations(annotations), m_interface(interface), m_documentHolder(document)
{
    m_pageSize.setWidth(FPDF_GetPageWidthF(m_page.data()));
    m_pageSize.setHeight(FPDF_GetPageHeightF(m_page.data()));
}

PdfPageAnnotationTask::~PdfPageAnnotationTask() = default;

void PdfPageAnnotationTask::run()
{
    if (m_interface.isCanceled()
        || PdfTaskQueue::instance().blockedId().contains(m_documentHolder->id())) {
        qDeleteAll(m_annotations);
        m_annotations.clear();

        m_interface.reportCanceled();
        return;
    }

    auto annotCount = FPDFPage_GetAnnotCount(m_page.data());
    if (m_annotations.size() == annotCount) {
        m_interface.reportFinished(&m_annotations);
        return;
    }

    auto annotation = new PdfAnnotation();

    auto currentAnnot = FPDFPage_GetAnnot(m_page.data(), m_annotations.size());
    auto annotSubtype = FPDFAnnot_GetSubtype(currentAnnot);
    annotation->setType(static_cast<PdfAnnotation::AnnotationType>(annotSubtype));

    FS_RECTF annotRect;
    FPDFAnnot_GetRect(currentAnnot, &annotRect);

    int convertedLeft = 0;
    int convertedTop = 0;
    FPDF_PageToDevice(m_page.data(), 0, 0, m_pageSize.width(), m_pageSize.height(), 0,
                      annotRect.left, qMax(annotRect.top, annotRect.bottom), &convertedLeft,
                      &convertedTop);
    int convertedRight = 0;
    int convertedBottom = 0;
    FPDF_PageToDevice(m_page.data(), 0, 0, m_pageSize.width(), m_pageSize.height(), 0,
                      annotRect.right, qMin(annotRect.bottom, annotRect.top), &convertedRight,
                      &convertedBottom);

    auto annotationHeight = qAbs(qAbs(convertedTop) - qAbs(convertedBottom));
    QRectF rect(0, 0, 0, 0);
    rect.setWidth(qAbs(annotRect.right - annotRect.left));
    rect.setHeight(annotationHeight);
    rect.moveTo({ static_cast<qreal>(convertedLeft),
                  m_pageSize.height() - static_cast<qreal>(convertedTop) - annotationHeight });

    annotation->setRect(rect);
    annotation->setLinkCoordinates(rect.center());

    auto getValue = [&currentAnnot](const QString &key) {
        auto length = FPDFAnnot_GetStringValue(currentAnnot, qUtf8Printable(key), nullptr, 0);
        QVector<ushort> buffer(static_cast<qint32>(length));
        FPDFAnnot_GetStringValue(currentAnnot, qUtf8Printable(key), buffer.data(),
                                 static_cast<quint32>(buffer.length()));
        auto annotString = QString::fromUtf16(buffer.data());
        return QVariant::fromValue(annotString);
    };

    static const auto allKeys = { PdfAnnotation::AnnotationKeys::Type,
                                  PdfAnnotation::AnnotationKeys::Subtype,
                                  PdfAnnotation::AnnotationKeys::Rect,
                                  PdfAnnotation::AnnotationKeys::Contents,
                                  PdfAnnotation::AnnotationKeys::P,
                                  PdfAnnotation::AnnotationKeys::NM,
                                  PdfAnnotation::AnnotationKeys::M,
                                  PdfAnnotation::AnnotationKeys::F,
                                  PdfAnnotation::AnnotationKeys::AP,
                                  PdfAnnotation::AnnotationKeys::AS,
                                  PdfAnnotation::AnnotationKeys::Border,
                                  PdfAnnotation::AnnotationKeys::C,
                                  PdfAnnotation::AnnotationKeys::StructParent,
                                  PdfAnnotation::AnnotationKeys::OC,
                                  PdfAnnotation::AnnotationKeys::Vertices,
                                  PdfAnnotation::AnnotationKeys::InkList,
                                  PdfAnnotation::AnnotationKeys::L,
                                  PdfAnnotation::AnnotationKeys::T };

    QVariantMap valuesMap;
    for (const auto &key : allKeys) {
        auto keyStringValue = PdfAnnotation::annotationKeyToQString(key);
        auto value = getValue(keyStringValue);
        valuesMap.insert(keyStringValue, value);
    }
    annotation->setValues(valuesMap);

    int inkListCount = FPDFAnnot_GetInkListCount(currentAnnot);
    QList<QList<QPointF>> points;
    for (int pathInd = 0; pathInd < inkListCount; ++pathInd) {
        QList<QPointF> path;
        int path_size = FPDFAnnot_GetInkListPath(currentAnnot, pathInd, nullptr, 0);
        std::vector<FS_POINTF> path_buffer(path_size);
        FPDFAnnot_GetInkListPath(currentAnnot, pathInd, path_buffer.data(), path_size);
        for (int pointInd = 0; pointInd < path_size; pointInd++)
            path.append(QPointF(path_buffer.at(pointInd).x, path_buffer.at(pointInd).y));

        points << path;
    }
    annotation->setPoints(points);

    auto annotCenterX = (annotRect.right + annotRect.right) / 2;
    auto annotCenterY = (annotRect.top + annotRect.bottom) / 2;
    auto annotLink = FPDFLink_GetLinkAtPoint(m_page.data(), annotCenterX, annotCenterY);
    auto annotDest = FPDFLink_GetDest(m_documentHolder->document().data(), annotLink);
    auto annotPageIndex = FPDFDest_GetDestPageIndex(m_documentHolder->document().data(), annotDest);
    annotation->setLinkToPage(annotPageIndex);

    FPDF_BOOL hasXVal, hasYVal, hasZoomVal;
    FS_FLOAT x, y, zoom;
    FPDFDest_GetLocationInPage(annotDest, &hasXVal, &hasYVal, &hasZoomVal, &x, &y, &zoom);
    annotation->setLinkPosition({ x, y });

    auto annotAction = FPDFLink_GetAction(annotLink);
    auto length =
            FPDFAction_GetURIPath(m_documentHolder->document().data(), annotAction, nullptr, 0);
    QByteArray buffer;
    buffer.resize(static_cast<qint32>(length));
    FPDFAction_GetURIPath(m_documentHolder->document().data(), annotAction, buffer.data(),
                          static_cast<quint32>(buffer.length()));
    auto annotUri = QString::fromLatin1(buffer.data());
    annotation->setUri(annotUri);

    quint32 a = 0;
    quint32 r = 0;
    quint32 g = 0;
    quint32 b = 0;
    FPDFAnnot_GetColor(currentAnnot, FPDFANNOT_COLORTYPE_Color, &r, &g, &b, &a);
    annotation->setColor(
            { static_cast<int>(r), static_cast<int>(g), static_cast<int>(b), static_cast<int>(a) });

    FPDFAnnot_GetColor(currentAnnot, FPDFANNOT_COLORTYPE_InteriorColor, &r, &g, &b, &a);
    annotation->setInteriorColor(
            { static_cast<int>(r), static_cast<int>(g), static_cast<int>(b), static_cast<int>(a) });

    QList<QPair<QColor, QColor>> objectsColors;
    auto objectCount = FPDFAnnot_GetObjectCount(currentAnnot);
    for (int i = 0; i < objectCount; ++i) {
        auto object = FPDFAnnot_GetObject(currentAnnot, i);
        if (object == nullptr)
            continue;

        FPDFPageObj_GetFillColor(object, &r, &g, &b, &a);
        QColor fillColor(r, g, b, a);

        FPDFPageObj_GetStrokeColor(object, &r, &g, &b, &a);
        QColor strokeColor(r, g, b, a);

        objectsColors.append({ fillColor, strokeColor });
    }
    annotation->setObjectsColors(objectsColors);

    m_annotations.append(annotation);

    // Not obvious implementation of yet another annotation request, but this is how it works
    auto *task = new PdfPageAnnotationTask(m_page, m_annotations, m_interface, m_documentHolder);
    auto addingResult = PdfTaskQueue::instance().addTask(task, PdfTaskQueue::TaskPriority::High, m_documentHolder->id());
    if (!addingResult) {
        delete task;
    }
}

int PdfPageAnnotationTask::id() const
{
    return m_documentHolder->id();
}

void PdfPageAnnotationTask::cancel() { }
