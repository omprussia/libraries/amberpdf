/****************************************************************************
**
** Copyright (C) 2021 - 2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the AmberPDF project.
**
** $QT_BEGIN_LICENSE:BSD$
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform LLC copyright holder nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QRectF>

#include <pdfium/fpdfview.h>
#include <pdfium/fpdf_text.h>

#include "pdftaskqueue.h"
#include "pdfdocumentholder.h"
#include "pdfword.h"
#include "pdfpagewordstask.h"

static QSet<QChar> s_endSymbols {' ', ',', '.', '.','\n', '\r','\x2', '\u0002', '!', '\t', '?', ':', ';', '(', ')', '[', ']', '{', '}'};


PdfPageWordsTask::PdfPageWordsTask(const QSharedPointer<fpdf_page_t__> &page,
                                   const QList<QObject *> &words,
                                   const QSizeF &originalSize,
                                   const QFutureInterface<QList<QObject *> > &interface,
                                   const QSharedPointer<PdfDocumentHolder> &document) :
    m_page(page),
    m_words(words),
    m_originalSize(originalSize),
    m_interface(interface),
    m_documentHolder(document)
{ }

PdfPageWordsTask::~PdfPageWordsTask() = default;

void PdfPageWordsTask::run()
{
    if (m_interface.isCanceled() || PdfTaskQueue::instance().blockedId().contains(m_documentHolder->id())) {
        QMutableListIterator<QObject *> annotationsIt(m_words);
        while (annotationsIt.hasNext())
            delete annotationsIt.next();

        m_interface.reportFinished(&m_words);
        return;
    }

    auto *textPage = FPDFText_LoadPage(m_page.data());
    if (!textPage) {
        m_interface.reportFinished(&m_words);
        return;
    }

    auto charCount = FPDFText_CountChars(textPage);

    double top, bottom, left, right;
    QString bufferString;
    QRectF rect;
    bool stringInit = false;
    PdfWord *actualWord = nullptr;
    double maxTop = 0.0;
    double minBottom = 0.0;

    for (int i = 0; i < charCount; ++i) {
        bool check = FPDFText_GetCharBox(textPage, i, &left, &right, &bottom, &top);
        if (!check)
            continue;

        QChar charValue = QChar(FPDFText_GetUnicode(textPage, i));

        maxTop = qMax(maxTop, top);
        minBottom = qMin(minBottom, bottom);

        if (s_endSymbols.contains(charValue)) {
            rect.setRight(right);
            rect.setBottom(maxTop);
            rect.setTop(minBottom);
            rect.setHeight(maxTop - minBottom);

            if (actualWord != nullptr) {
                actualWord->setIndex(i);
                actualWord->setRect(rect);
                actualWord->setValue(bufferString);
            }

            stringInit = false;
        } else {
            if (!stringInit) {
                minBottom = m_originalSize.height();
                maxTop = 0.0;

                rect.setLeft(left);

                m_words.append(new PdfWord());
                actualWord = qobject_cast<PdfWord *>(m_words.last());

                bufferString.clear();

                stringInit = true;
            }

            maxTop = qMax(maxTop, top);
            minBottom = qMin(minBottom, bottom);
            bufferString.append(charValue);
        }
    }

    FPDFText_ClosePage(textPage);

    m_interface.reportFinished(&m_words);
}

int PdfPageWordsTask::id() const
{
    return m_documentHolder->id();
}

void PdfPageWordsTask::cancel() {  }
