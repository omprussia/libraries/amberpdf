/****************************************************************************
**
** Copyright (C) 2021 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the AmberPDF project.
**
** $QT_BEGIN_LICENSE:BSD$
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform LLC copyright holder nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QFuture>
#include <QThread>

#include <pdfium/fpdfview.h>

#include "pdfworker.h"
#include "tasks/pdftask.h"
#include "pdftaskqueue.h"

PdfTaskQueue::PdfTaskQueue(QObject *parent) : QObject(parent)
{
    FPDF_InitLibrary();

    qRegisterMetaType<QFuture<QList<QObject *>>>("QFuture<QList<QObject *>>");
    m_workerThread = new QThread(this);
    m_worker = new PdfWorker();

    m_worker->moveToThread(m_workerThread);

    connect(this, &PdfTaskQueue::newTaskAdded, m_worker, &PdfWorker::work);

    m_workerThread->start();
}

PdfTaskQueue::~PdfTaskQueue()
{
    m_workerThread->exit();

    if (m_worker != nullptr)
        m_worker->deleteLater();

    FPDF_DestroyLibrary();
}

PdfTaskQueue &PdfTaskQueue::instance()
{
    static PdfTaskQueue instance;
    return instance;
}

bool PdfTaskQueue::addTask(PdfTask *task, TaskPriority priority, int id)
{
    if (m_blockedIds.contains(id))
        return false;

    if (priority == TaskPriority::Immediately) {
        if (m_eliteTask != nullptr)
            return false;

        m_eliteTask = task;
        emit newTaskAdded();
        return true;
    }

    if (priority == TaskPriority::High)
        m_highPriorityQueue.enqueue(task);
    else
        m_lowPriorityQueue.enqueue(task);

    emit newTaskAdded();

    return true;
}

PdfTask *PdfTaskQueue::getTask()
{
    static quint64 s_taskCount = 0;
    PdfTask *task = nullptr;

    if (m_eliteTask != nullptr) {
       qSwap(task, m_eliteTask);
       return task;
    }

    if (++s_taskCount % m_priorityRatio == 0) {
        if (!m_lowPriorityQueue.isEmpty()) {
            task = m_lowPriorityQueue.dequeue();
        } else if (!m_highPriorityQueue.isEmpty()) {
            task = m_highPriorityQueue.dequeue();
        }
    } else {
        if (!m_highPriorityQueue.isEmpty()) {
            task = m_highPriorityQueue.dequeue();
        } else if (!m_lowPriorityQueue.isEmpty()) {
            task = m_lowPriorityQueue.dequeue();
        }
    }

    return task;
}

int PdfTaskQueue::size() const
{
    return m_lowPriorityQueue.size() + m_highPriorityQueue.size() + (m_eliteTask == nullptr ? 0 : 1);
}

void PdfTaskQueue::addBlockedId(int id)
{
    m_blockedIds.insert(id);
}

QSet<int> PdfTaskQueue::blockedId() const
{
    return m_blockedIds;
}

void PdfTaskQueue::setPrioritRatio(int ratio)
{
    m_priorityRatio = ratio;
}

