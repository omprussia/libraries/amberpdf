/****************************************************************************
**
** Copyright (C) 2021 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the AmberPDF project.
**
** $QT_BEGIN_LICENSE:BSD$
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform LLC copyright holder nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef PDFDOCUMENT_H
#define PDFDOCUMENT_H

#include <QObject>
#include <QFuture>
#include <QSharedPointer>
#include "pdfword.h"
#include "pdfbookmark.h"
#include "pdfpage.h"

class PdfPage;
class PdfWord;
class PdfDocumentPrivate;
class PdfDocument : public QObject
{
    Q_OBJECT

    Q_PROPERTY(DocumentStatus status READ status NOTIFY statusChanged)
    Q_PROPERTY(int pageCount READ pageCount NOTIFY pageCountChanged)
    Q_PROPERTY(QString path READ path NOTIFY pathChanged)
    Q_PROPERTY(int fileVersion READ fileVersion NOTIFY fileVersionChanged)

public:
    // NOTE: values https://pdfium.googlesource.com/pdfium/+/refs/heads/master/public/fpdfview.h#571
    enum DocumentStatus {
        Success,
        Unknown,
        FileError,
        FormatError,
        PasswordError,
        SecurityError,
        ContentError,
        Loading = 99,
        InternalError
    };
    Q_ENUM(DocumentStatus)


    explicit PdfDocument(QObject *parent = nullptr);
    virtual ~PdfDocument();

    DocumentStatus status() const;
    int pageCount() const;
    QString path() const;
    QFuture<QSizeF> pageSize(int index) const;
    QFuture< QSharedPointer<PdfPage> > page(int pageIndex) const;
    void loadDocument(const QString &fileName, const QString &password = QString());
    void loadDocument(const QByteArray &buffer, const QString &password = QString());
    QFuture<QVector<PdfBookmark> > bookmarks() const;
    int fileVersion() const;
    bool saveDocumentAs(const QString &path) const;
    bool exportDocument(QIODevice *output) const;

signals:
    void statusChanged(PdfDocument::DocumentStatus status);
    void pageCountChanged(int pageCount);
    void pathChanged(QString path);
    void fileVersionChanged(int version);

private:
    Q_DISABLE_COPY(PdfDocument)
    Q_DECLARE_PRIVATE(PdfDocument);

    PdfDocumentPrivate * const d_ptr;
};

#endif // PDFDOCUMENT_H
