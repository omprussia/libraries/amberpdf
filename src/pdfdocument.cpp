/****************************************************************************
**
** Copyright (C) 2021 - 2022 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the AmberPDF project.
**
** $QT_BEGIN_LICENSE:BSD$
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform LLC copyright holder nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QFileInfo>
#include <QFutureWatcher>
#include <pdfpagewordstask.h>

#include "pdftaskqueue.h"
#include "tasks/pdfdocumentloadtask.h"
#include "tasks/pdfdocumentloadfrommemtask.h"
#include "tasks/pdfdocumentpagesizetask.h"
#include "tasks/pdfdocumentpagecounttask.h"
#include "tasks/pdfpageloadtask.h"
#include "tasks/pdfdocumentbookmarksloadtask.h"
#include "tasks/pdfdocumentversiongettask.h"
#include "tasks/pdfdocumentsavetask.h"
#include "tasks/pdfdocumentexporttask.h"
#include "pdfdocument.h"
#include "pdfword.h"
#include "pdfpage.h"
#include "pdfdocumentholder.h"
#include "pdfdocument_p.h"


void PdfDocumentPrivate::clear()
{
    if (m_documentHolder) {
        PdfTaskQueue::instance().addBlockedId(m_documentHolder->id());

        if (!m_openPages.empty()) {
            for (auto &future : m_openPages) {
                future.cancel();
                if (!future.isCanceled())
                    future.result().clear();
            }
            m_openPages.clear();
        }
    }

    m_documentHolder.reset();
    m_status = PdfDocument::DocumentStatus::Unknown;
}

void PdfDocumentPrivate::handleDocument(QPair<int, QSharedPointer<fpdf_document_t__>> loadResult)
{
    m_status = loadResult.second  ? PdfDocument::DocumentStatus::Success
                                     : static_cast<PdfDocument::DocumentStatus>(loadResult.first);

    if (m_status != PdfDocument::DocumentStatus::Success)
        return;

    m_documentHolder.clear();
    m_documentHolder = QSharedPointer<PdfDocumentHolder>(new PdfDocumentHolder(loadResult.second));
    if (!m_documentHolder) {
        m_status = PdfDocument::DocumentStatus::InternalError;
        return;
    }

    m_status = PdfDocument::DocumentStatus::Success;
}


PdfDocument::PdfDocument(QObject *parent) : QObject(parent),
    d_ptr(new PdfDocumentPrivate())
{
    connect(this, &PdfDocument::statusChanged, this, [&](DocumentStatus status) {
        if (status != DocumentStatus::Success)
            return;

        Q_D(PdfDocument);
        QFutureInterface<int> pageCountInterface;
        d->m_pageCountWatcher.setFuture(pageCountInterface.future());
        auto *pageCountTask = new PdfDocumentPageCountTask(d->m_documentHolder, pageCountInterface);
        auto addingResult = PdfTaskQueue::instance().addTask(pageCountTask, PdfTaskQueue::TaskPriority::High, d->m_documentHolder->id());
        if (!addingResult) {
            delete pageCountTask;
            d->m_status = DocumentStatus::InternalError;
            emit statusChanged(d->m_status);
        }

        QFutureInterface<int> versionInterface;
        d->m_versionWatcher.setFuture(versionInterface.future());
        auto *versionTask = new PdfDocumentVersionGetTask(d->m_documentHolder, versionInterface);
        addingResult = PdfTaskQueue::instance().addTask(versionTask, PdfTaskQueue::TaskPriority::High, d->m_documentHolder->id());
        if (!addingResult) {
            delete versionTask;
            d->m_status = DocumentStatus::InternalError;
            emit statusChanged(d->m_status);
        }
    });

    connect(&d_ptr->m_pageCountWatcher, &QFutureWatcher<int>::finished, this, [&]() {
        Q_D(PdfDocument);
        if (d->m_pageCountWatcher.isCanceled())
            return;

        auto pageCount = d->m_pageCountWatcher.result();
        d_ptr->m_pagesCount = pageCount;
        emit pageCountChanged(d_ptr->m_pagesCount);
    });

    connect(&d_ptr->m_versionWatcher, &QFutureWatcher<int>::finished, this, [&]() {
        Q_D(PdfDocument);
        if (d->m_versionWatcher.isCanceled())
            return;

        d_ptr->m_fileVersion = d->m_versionWatcher.result();
        emit fileVersionChanged(d_ptr->m_fileVersion);
    });
}

PdfDocument::~PdfDocument()
{
    Q_D(PdfDocument);
    d->clear();

    delete d_ptr;
}

PdfDocument::DocumentStatus PdfDocument::status() const
{
    Q_D(const PdfDocument);
    return d->m_status;
}

int PdfDocument::pageCount() const
{
    Q_D(const PdfDocument);

    if (!d->m_documentHolder)
        return -1;

    if (d->m_pageCountWatcher.isFinished() && !d->m_pageCountWatcher.isCanceled())
        return d->m_pageCountWatcher.result();

    return -1;
}

QString PdfDocument::path() const
{
    Q_D(const PdfDocument);
    return d->m_path;
}

QFuture< QSharedPointer<PdfPage> > PdfDocument::page(int pageIndex) const
{
    Q_D(const PdfDocument);

    if (pageIndex < 0 || pageIndex > pageCount())
        return {  };

    if (!d->m_documentHolder || status() != DocumentStatus::Success)
        return {  };

    if (!d->m_openPages.contains(pageIndex)) {
        QFutureInterface<QSharedPointer<PdfPage>> interface;
        auto *loadPageTask = new PdfPageLoadTask(d->m_documentHolder, interface, pageIndex);
        auto addingResult = PdfTaskQueue::instance().addTask(loadPageTask, PdfTaskQueue::TaskPriority::High, d->m_documentHolder->id());
        if (!addingResult) {
            delete loadPageTask;
            return {  };
        }

        interface.reportStarted();

        d->m_openPages.insert(pageIndex, interface.future());
    }

    return d->m_openPages.value(pageIndex);
}

QFuture<QSizeF> PdfDocument::pageSize(int pageIndex) const
{
    Q_D(const PdfDocument);

    if (pageIndex < 0 || pageIndex > pageCount())
        return {  };

    if (!d->m_documentHolder || status() != DocumentStatus::Success)
        return {  };

    QFutureInterface<QSizeF> interface;
    auto *loadPageTask = new PdfDocumentPageSizeTask(d->m_documentHolder, interface, pageIndex);
    auto addingResult = PdfTaskQueue::instance().addTask(loadPageTask, PdfTaskQueue::TaskPriority::High, d->m_documentHolder->id());
    if (!addingResult) {
        delete loadPageTask;
        return {  };
    }

    interface.reportStarted();

    return interface.future();
}

void PdfDocument::loadDocument(const QString &fileName, const QString &password)
{
    Q_D(PdfDocument);

    if (d->m_status == DocumentStatus::Success)
        d->clear();

    d->m_status = DocumentStatus::Loading;
    emit statusChanged(d->m_status);

    d->m_path = fileName;
    emit pathChanged(d->m_path);

    QFileInfo fileInfo(fileName);
    if (!fileInfo.isReadable()) {
        d->m_status = DocumentStatus::FileError;
        emit statusChanged(d->m_status);
        return;
    }

    auto pdfiumLoadFuture = d->fromPdf(fileName, password);
    if (pdfiumLoadFuture.isFinished()) {
        d->handleDocument(pdfiumLoadFuture.result());
        emit statusChanged(d->m_status);
        return;
    }

    auto pdfLoadWatcher = new QFutureWatcher<QPair<int, QSharedPointer<fpdf_document_t__>>>(this);
    pdfLoadWatcher->setFuture(pdfiumLoadFuture);

    connect(pdfLoadWatcher, &QFutureWatcher<QPair<int, QSharedPointer<fpdf_document_t__>>>::finished, this, [&]() {
        Q_D(PdfDocument);
        auto watcher = dynamic_cast<QFutureWatcher<QPair<int, QSharedPointer<fpdf_document_t__>>>*>(sender());
        if (watcher == nullptr)
            return;

        if (watcher->isCanceled())
            d->m_status = DocumentStatus::InternalError;
        else
            d->handleDocument(watcher->result());

        emit statusChanged(d->m_status);
        watcher->deleteLater();
    });
}

void PdfDocument::loadDocument(const QByteArray &buffer, const QString &password)
{
    Q_D(PdfDocument);

    if (d->m_status == DocumentStatus::Success)
        d->clear();

    d->m_status = DocumentStatus::Loading;
    emit statusChanged(d->m_status);

    auto pdfiumLoadFuture = d->fromMem(buffer, password);
    if (pdfiumLoadFuture.isFinished()) {
        d->handleDocument(pdfiumLoadFuture.result());
        emit statusChanged(d->m_status);
        return;
    }

    auto pdfLoadWatcher = new QFutureWatcher<QPair<int, QSharedPointer<fpdf_document_t__>>>(this);
    pdfLoadWatcher->setFuture(pdfiumLoadFuture);

    connect(pdfLoadWatcher, &QFutureWatcher<QPair<int, QSharedPointer<fpdf_document_t__>>>::finished, this, [&]() {
        Q_D(PdfDocument);
        auto watcher = dynamic_cast<QFutureWatcher<QPair<int, QSharedPointer<fpdf_document_t__>>>*>(sender());
        if (watcher == nullptr)
            return;

        if (watcher->isCanceled())
            d->m_status = DocumentStatus::InternalError;
        else
            d->handleDocument(watcher->result());

        emit statusChanged(d->m_status);
        watcher->deleteLater();
    });
}

QFuture<QVector<PdfBookmark> > PdfDocument::bookmarks() const
{
    Q_D(const PdfDocument);
    QFutureInterface<QVector<PdfBookmark>> interface;
    auto *task = new PdfDocumentBookmarksLoadTask(d->m_documentHolder, interface);

    auto addResult = PdfTaskQueue::instance().addTask(task, PdfTaskQueue::TaskPriority::High);
    if (!addResult) {
        delete task;
        return {  };
    }

    interface.reportStarted();

    return interface.future();
}

int PdfDocument::fileVersion() const
{
    Q_D(const PdfDocument);
    return d->m_fileVersion;
}

bool PdfDocument::saveDocumentAs(const QString &path) const
{
    Q_D(const PdfDocument);
    QFutureInterface<bool> interface;
    auto *task = new PdfDocumentSaveTask(path, interface, d->m_documentHolder->document());

    auto addResult = PdfTaskQueue::instance().addTask(task, PdfTaskQueue::TaskPriority::Immediately);
    if (!addResult) {
        delete task;
        return false;
    }

    interface.reportStarted();

    return interface.future().result();
}

bool PdfDocument::exportDocument(QIODevice *output) const
{
    Q_D(const PdfDocument);
    QFutureInterface<bool> interface;
    auto *task = new PdfDocumentExportTask(output, interface, d->m_documentHolder->document());

    auto addResult = PdfTaskQueue::instance().addTask(task, PdfTaskQueue::TaskPriority::Immediately);
    if (!addResult) {
        delete task;
        return false;
    }

    interface.reportStarted();

    return interface.future().result();
}

QFuture<QPair<int, QSharedPointer<fpdf_document_t__>>> PdfDocumentPrivate::fromPdf(const QString &fileName, const QString &password)
{
    QFutureInterface<QPair<int, QSharedPointer<fpdf_document_t__>>> interface;
    auto *task = new PdfDocumentLoadTask(fileName, password, interface);

    auto addResult = PdfTaskQueue::instance().addTask(task, PdfTaskQueue::TaskPriority::High);

    if (!addResult)
        delete task;

    interface.reportStarted();

    return interface.future();
}

QFuture<QPair<int, QSharedPointer<fpdf_document_t__>>> PdfDocumentPrivate::fromMem(const QByteArray &buffer, const QString &password)
{
    QFutureInterface<QPair<int, QSharedPointer<fpdf_document_t__>>> interface;
    auto *task = new PdfDocumentLoadFromMemTask(buffer, password, interface);

    PdfTaskQueue::instance().addTask(task, PdfTaskQueue::TaskPriority::High);
    interface.reportStarted();

    return interface.future();
}
