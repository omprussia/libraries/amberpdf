# AmberPDF

PDFium is the PDF generation and rendering [library](https://pdfium.googlesource.com/pdfium/).
This project is a PDFium wrapper that allows to use it with Qt.

Build status:

master - [![pipeline status](https://gitlab.com/omprussia/libraries/amberpdf/badges/master/pipeline.svg)](https://gitlab.com/omprussia/libraries/amberpdf/-/commits/master)

The source code of the project is provided under
[the license](LICENSE.BSD-3-CLAUSE.md),
that allows it to be used in third-party applications.

The [contributor agreement](CONTRIBUTING.md)
documents the rights granted by contributors to the Open Mobile Platform.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules
of the Open Mobile Platform which informs you how we expect
the members of the community will interact while contributing and communicating.

For information about contributors see [AUTHORS](AUTHORS.md).


## Project Structure

The project has a common structure
of an application based on C++ and QML for Aurora OS.

All c++ sources files are in [src](src) directory.
